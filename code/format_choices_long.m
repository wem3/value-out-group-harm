function [long] = format_choices_long(repository, writeOut)
% FORMAT_CHOICES_LONG.M %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% change choice frequencies from wide into long format
%
% USAGE: [choices] = format_choices_long(bandit, expt, addVars, writeOut)
%
% INPUT:
%
%       repository = path to top level directory for gitlab repository
%
%
%       writeOut = logical indicating whether or not to write out data
%                   (defaults to 0)
%
% OUTPUT:
%
%       long = table w/ choices by arm in long format
%       
%       all_choices.csv for easy import to R, python, etc.
%
% written: ~#wem3#~ [20190213]
% edited:  ~#wem3#~ [20191204]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if writeOut isn't passed, set to 0
if nargin < 2
    writeOut = 0;
end

% load the .mat with multi-experiment structure
load(matFile);

% make an empty table for 'long'
long = table();

% loop over experiments in eList
for study = 1:3
    % load the data for this study
    load(fullfile(pwd, 'data', 'inputs',['study_', num2str(study), '_data.mat']));
    % assign the 'subject' table of the bandit structure to 'data'
    data = bandit.subject;
    % in case the dataset hasn't been categoricalized...
    data.subjid  = categorical(data.subjid);
    data.expt   = categorical(data.expt);
    data.partner = categorical(data.partner);
    % stack it 4 times
    subjid  = repmat(data.subjid, 4, 1);
    partner = repmat(data.partner, 4, 1);
    expt    = repmat(data.expt, 4, 1);
    % collapse choice frequencies into long vector
    freqs = [data.chose_80; data.chose_60; data.chose_40; data.chose_20];
    % create label vector for gem/earn percentage
    label = [repmat(80,height(data),1);
             repmat(60,height(data),1);
             repmat(40,height(data),1);
             repmat(20,height(data),1)];
    label = categorical(label);

    % make a new table in long form
    tmp = table(subjid, expt, partner, freqs, label);
    % append to big ass table
    long = [long; tmp];
    clear bandit data
end

% write new table
if writeOut == 1
    fileName = fullfile(repository,'results','combined_choices_long_sgitlab repositorys_1234.csv');
    writetable(long, fileName,'Delimiter','comma');
end
