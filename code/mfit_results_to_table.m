function [all_data, all_choices] = mfit_results_to_table(repository)
% MFIT_RESULTS_TO_TABLE.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function to parse mfit output across all three studies
% and collapse into a table that can be written to .csv
%
%   DEPENDENCIES: outputs from ~/code/mfit_single_model.m
%
%           ~/data/outputs/study_1_mfit.mat
%           ~/data/outputs/study_2_mfit.mat
%           ~/data/outputs/study_3_mfit.mat
%
%   INPUTS: repository = string with path to gitlab repository
%
%   OUTPUTS: all_data    = matlab table with results from all 3 studies
%            all_choices = matlab table with choice frequencies in long format
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   author:          wem3
%   written:         191111
%   modified:        191121
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% make empty tables so we can stack output across experiments
    all_data   = table();
    all_choices = table();
    % loop over studies to make combined table
    for study   = 1:3
        load(fullfile(repository,'data','outputs',['study_', num2str(study), '_mfit.mat']));
        numSubs = length(bandit.data);
        % make an empty table
        tmp = readtable(fullfile(repository,'data','inputs',['study_', num2str(study), '_subject.csv']));
        % append string with study number (so we can stack all the tables)
        tmp.expt = categorical(repmat(study,numSubs,1));
        % append preference for gems/earning parameter
        tmp.w = bandit.results.x(:,4);
        % add variable to indicate political party (studies 2 & 3)
        if ismember('party', tmp.Properties.VariableNames)
            tmp.party  = categorical(tmp.party);
        else
            tmp.party  = categorical(repmat({'none'},numSubs,1));
        end
        % parse out variables of interest
        tmp = tmp(:,...
            {'subjid','expt','gender','party','w','idOut','idIn',...
            'chose_80','chose_60','chose_40','chose_20'});
        % stack tables for bandit_table output
        all_data = [all_data; tmp];
        % make long format .csv with choice frequencies
        subjid = repmat(tmp.subjid, 4, 1);
        expt   = repmat(tmp.expt, 4, 1);
        % collapse choice frequencies into one long vector
        freqs  = [tmp.chose_80; tmp.chose_60; tmp.chose_40; tmp.chose_20];
        % create label vector for gem/earn percentage
        label = [repmat(80,height(tmp),1);
                 repmat(60,height(tmp),1);
                 repmat(40,height(tmp),1);
                 repmat(20,height(tmp),1)];
        label = categorical(label);
        long = table(subjid, expt, freqs, label);
        all_choices = [all_choices; long];
    end
    % categoricalize variables (necessary/useful for .csv output?)
    all_data.subjid = categorical(all_data.subjid);
    all_data.gender = categorical(all_data.gender);
    all_choices.subjid = categorical(all_choices.subjid);
    % write out new tables
    dataTableName = fullfile(repository,'data','outputs',['all_data.csv']);
    writetable(all_data,dataTableName);
    choiceTableName = fullfile(repository,'data','outputs',['all_choices.csv']);
    writetable(all_choices,choiceTableName);
    return
