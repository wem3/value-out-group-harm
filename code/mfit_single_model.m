function [bandit] = mfit_single_model(study, respository)
% MFIT_SINGLE_MODEL.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 1. calls mfit_optimize_hierarchical.m for a single model for one study
% 2. simulates data based on mfit output
% 3. calls mfit_optimize_hierarchical.m on the simulated data
%
%   DEPENDENCIES:
%
%       MATLAB R2018b (9.5.0.944444)
%
%       mfit toolkit (https://github.com/sjgershm/mfit)
%
%       study_<study>_data.mat:
%           
%               MATLAB structure with data formatted for mfit toolkit
%
%   INPUTS:
%
%       study: scalar indicating study for which to perform optimization [1 - 3]
%
%       repository: string with path to gitlab repository with data & code
%
%   OUTPUTS:
%
%       bandit: recursive MATLAB structure with the following fields:
%
%               data:       original input data
%               results:    output from mfit_optimize_hierarchical on data
%               simdata:    data simulated based on output in results
%               simrez:     output from mfit_optimize_hierarchcial on simdata
%
%       saved out as .mat file to:
%                              <repository>/data/outputs/study_<study>_mfit.mat    
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   author:          wem3
%   written:         180430
%   modified:        191120
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % set path to include custom functions and mfit toolkit
    addpath('~/matlab');addpath('~/matlab/sam/mfit');addpath(genpath(respository))
    % initialize parallel pool (changed to fix job output race condition) ~#wem3#~ [2019-07-30]
    pc = parcluster('local');
    pc.JobStorageLocation = fullfile(respository,'jobs');
    parpool(pc, 4);
    % load structure containing behavioral data
    load(fullfile(respository,'data','inputs',['study_', num2str(study), '_data.mat']));
    % inputs for mfit to set number of starts & parallelization
    nstarts = 5;        % set to 1 for test mode, adjust for actual computation
    isparallel = 1;
    % run optimization on subject data
    disp(['Fitting Study ', num2str(study), ' with ', num2str(nstarts), ' starts and ', num2str(length(data)), ' subjects.'])
    params = set_opts;
    f = eval(['@(x,data) qlik(x,data)']);
    results = mfit_optimize_hierarchical(f,params,data,nstarts,isparallel);
    % generate simulated data based on results from mfit_optimize_hierarchical
    disp(['Simulating data for Study', num2str(study), ' .'])
    for sCount = 1:length(data)
        simdata(sCount) = qsim(results.x(sCount,:),data(sCount));
    end
    % run optimization on simulated data
    disp(['Fitting simulated data for Study', num2str(study), ', with ', num2str(nstarts), ' starts and ', num2str(length(simdata)), ' subjects.'])
    simf = eval(['@(x,simdata) qlik(x,simdata)']);
    simrez = mfit_optimize_hierarchical(simf,params,simdata,nstarts,isparallel);    
    % put data, results, simdata, and simrez into a bandit structure
    bandit.data = data;
    bandit.simdata = simdata;
    bandit.results = results;
    bandit.simrez = simrez;
    % save out results
    saveName = fullfile(respository,'data','outputs',['study_', num2str(study), '_mfit.mat']);
    save(saveName,'bandit')
    % kill the workers, man!
    delete(gcp);
return
