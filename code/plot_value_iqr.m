function plot_value_iqr(study)
% PLOT_VALUE_IQR.m%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function to get weighted action values and plot by quartile
%
% INPUTS:
%       -study: scalar [1,2,3] to indicate study in question
%
% OUTPUTS:
%       -./figs/components/Study_<study>_value_by_trial.png 
%
% written: wem3 [20191101]
% edited:  wem3 [20191204]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% load mfit output for this study
load(fullfile(pwd, 'data', 'outputs', ['study_', num2str(study), '_mfit.mat']));
% cell string with quartile list
qList = {'q1','q2','q3','q4'};
% list to name doors/arms
dList = {'p80','p60','p40','p20'};
% gems/earn probabilities per door
pDoors = [0.8 0.6 0.4 0.2];
% extract preference for gems/earning
w = bandit.results.x(:,4);

% create indices for subjects in each quartile
values.q1.subs = find(w<(mean(w)-iqr(w)));
values.q2.subs = find((mean(w)-iqr(w)) < w & w < mean(w));
values.q3.subs = find((mean(w) < w & w < mean(w) + iqr(w)));
values.q4.subs = find((w > (mean(w)+iqr(w))));

% loop over qList
for qCount = 1:length(qList)
    % loop over dList
    for dCount = 1:length(dList);
        % make an empty vector for each subject in each quartile
        values.(qList{qCount}).(dList{dCount}) = nan(360,length(values.(qList{qCount}).subs));
        % loop over subjects in each quartile
        for sCount = 1:length(values.(qList{qCount}).subs)
            subj = values.(qList{qCount}).subs(sCount);
            door = bandit.data(subj).D;
            % extract weighted action values for each door/arm
            values.(qList{qCount}).(dList{dCount})(:,sCount) = ...
            bandit.results.latents(subj).v(:, door == pDoors(dCount));
        end
        % get mean and standard error for each quartile
        y.(qList{qCount}).(dList{dCount}).avg = mean(values.(qList{qCount}).(dList{dCount}),2);
        for tCount = 1:length(y.(qList{qCount}).(dList{dCount}).avg)
            y.(qList{qCount}).(dList{dCount}).sem(1,tCount) = ...
            std(values.(qList{qCount}).(dList{dCount})(tCount,:))./sqrt(size(values.(qList{qCount}).subs,1));
        end
    end
end

% plot that shit!

figure('Renderer', 'painters', 'Position', [10 10 1000 800]);

subplot(2,1,1);
q1 = shadedErrorBar([1:360],y.q1.p80.avg,y.q1.p80.sem,'lineprops','-r'); hold on;
q1.mainLine.LineWidth = 2;
q2 = shadedErrorBar([1:360],y.q2.p80.avg,y.q2.p80.sem,'lineprops','-g'); hold on;
q2.mainLine.LineWidth = 2;
q3 = shadedErrorBar([1:360],y.q3.p80.avg,y.q3.p80.sem,'lineprops','-b'); hold on;
q3.mainLine.LineWidth = 2;
q4 = shadedErrorBar([1:360],y.q4.p80.avg,y.q4.p80.sem,'lineprops','-k'); hold on;
q4.mainLine.LineWidth = 2;
grid on
set(gca,'FontSize',16,'YLim',[0 .35],'XLim',[0 360],'YAxisLocation','origin');
ylabel('weighted action value','FontSize',16);
xlabel('trial','FontSize',16);
lgd=legend({'q1','q2','q3','q4'},'Location','eastoutside');
title(lgd,'w by quartile');
tString = ['Study ', num2str(study), ': 80% earn arm'];
title(tString);

subplot(2,1,2);
q1 = shadedErrorBar([1:360],y.q1.p20.avg,y.q1.p20.sem,'lineprops','-r'); hold on;
q1.mainLine.LineWidth = 2;
q2 = shadedErrorBar([1:360],y.q2.p20.avg,y.q2.p20.sem,'lineprops','-g'); hold on;
q2.mainLine.LineWidth = 2;
q3 = shadedErrorBar([1:360],y.q3.p20.avg,y.q3.p20.sem,'lineprops','-b'); hold on;
q3.mainLine.LineWidth = 2;
q4 = shadedErrorBar([1:360],y.q4.p20.avg,y.q4.p20.sem,'lineprops','-k'); hold on;
q4.mainLine.LineWidth = 2;
grid on
set(gca,'FontSize',16,'YLim',[0 .35],'XLim',[0 360],'YAxisLocation','origin');
ylabel('weighted action value','FontSize',16);
xlabel('trial','FontSize',16);
lgd=legend({'q1','q2','q3','q4'},'Location','eastoutside');
title(lgd,'w by quartile');
tString = ['Study ', num2str(study), ': 20% earn arm'];
title(tString);

hold off

fileName = fullfile(pwd, 'figs', 'components', ['Study_', num2str(study), '_value_by_trial.png']);
saveas(gcf, fileName)
