function [lik, latents] = qlik(x, data)
% QLIK.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Likelihood function for Q-learning algorithm  used to fit models to data from
% 4-armed bandit task w/ qualitatively distinct reward outcome types:
%                                              (earn vs. burn || earn vs. burn)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% USAGE: [lik, latents] = qlik(x, data)
%
% DEPENDENCIES:
%   mfit toolkit: "Simple model-fitting tools."
%       - https://github.com/sjgershm/mfit
%
%   ~/inputs/study_<study>_data.mat: structure with mfit formatted data
%
% INPUTS:
%   x - parameters: 
%       x(1) - inverse temperature   [randomness of behavior w/rt model]
%       x(2) - stickiness            [perseveration i.e., repeated keypresses]
%       x(3) - learning rate         [influence of prediction error on value update]
%       x(4) - preference weight     [tendency to value earn/burn more highly]
%
%           ~# NOTE: The parameters in x are what `mfit_optimize.m` estimates. 
%              This function isn't called directly, nor is x directly input. #~
%
%   data - structure with the following REQUIRED fields:
%       .N - number of trials
%       .c - [N x 1] choices
%       .r - [N x 2] rewards [earn/earn, burn/burn]
%       .d - [1 x 4] probability door returns earn/earn
%
%       - additional fields in data not needed for this function
%       .subID   - sequentially assigned string         
%       .pReward - [N x 4] probability of reward for each arm
%       .cFreqs  - choice frequencies for each door/machine
%                  sorted by arms with pearn/pEarn = [.8 .6 .4 .2]
%       .pGain   - behaviorally derived preference for earn/earn
%                  summed frequency of arm choices * probability reward is earn/earn
%
% OUTPUTS:
%
%   lik     = scalar with log likelihood of model
%   latents = structure with per-trial latent variables
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   author:          wem3
%   written:         180430
%   modified:        1901121
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    it = x(1);
    k  = x(2);
    lr = x(3);
    w  = x(4);
    % initialize separate values for earn, burn, u (indexes last choice history) 
    qEarn = [.25 .25 .25 .25]; % initial earn values
    qBurn = [.25 .25 .25 .25]; % initial burn values
    u = zeros(1,4);  % stickiness
    lik = 0;
    % loop over trials
    for n = 1:data.N
        c = data.c(n);                      % choice for this trial
        r = data.r(n,:);                    % reward outcomes for this trial
        v = w*qEarn + (1-w)*qBurn;                % preference weighted values
        q = it*v + k*u;                     % action values
        p = q(c) - logsumexp(q);            % softmax probabilities
        lik = lik + p;    % compute likelihood via soft
        % calculate separate reward prediction errors for earn & burn
        earnPE = r(1)-qEarn(c);
        burnPE = r(2)-qBurn(c);
        % cache latent values from this trial
        latents.qEarn(n,:) = qEarn;
        latents.qBurn(n,:) = qBurn;
        latents.v(n,:)  = v;
        latents.q(n,:)  = q;
        latents.p(n,:)  = p;
        latents.earnPE(n,1)  = earnPE;
        latents.burnPE(n,1)  = burnPE;
        % update earn/burn values w/ learning rate * prediction error
        qEarn(c) = qEarn(c) + lr*earnPE;
        qBurn(c) = qBurn(c) + lr*burnPE;
        % update stickiness
        u = zeros(1,4); u(c) = 1;
    end
