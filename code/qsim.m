function simdata = qsim(x, data)
% QSIM.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function to generate simulated data for qlik.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   author:          wem3
%   written:         191107
%   modified:        191121
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    it = x(1);
    k  = x(2);
    lr = x(3);
    w  = x(4);
    simdata.N = data.N;
    simdata.C = data.C;
    simdata.D = data.D;
    simdata.pReward = data.pReward;
    % initialize separate values for earn, burn, u (indexes last choice history) 
    qEarn = [.25 .25 .25 .25]; % initial earn values
    qBurn = [.25 .25 .25 .25]; % initial burn values
    u = zeros(1,4);  % stickiness
    % loop over trials
    for n = 1:data.N
        v = w*qEarn + (1-w)*qBurn;                % preference weighted values
        q = it*v + k*u;                     % action values
        p = exp(q - logsumexp(q));          % softmax probabilities
        c = fastrandsample(p);
        r = [0 0];
        % determine rewards
        if data.pReward(n,c) > rand
            if data.D(c) > rand
                r(1) = 1;
            else
                r(2) = 1;
            end
        end
        simdata.c(n,1) = c;
        simdata.r(n,:) = r;
        % calculate separate reward prediction errors for earn & burn
        earnPE = r(1)-qEarn(c);
        burnPE = r(2)-qBurn(c);
        % update earn/burn values w/ learning rate * prediction error
        qEarn(c) = qEarn(c) + lr*earnPE;
        qBurn(c) = qBurn(c) + lr*burnPE;
        % update stickiness
        u = zeros(1,4); u(c) = 1;
    end
