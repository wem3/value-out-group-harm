function param = set_opts
% SET_OPTS.M %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function to create parameter structure for mfit
%
% OUTPUTS: param - structure to use as input to mfit_optimize_hierarchical.m
%
%       param(1) = inverse temperature
%       param(2) = stickiness (perseveration parameter)
%       param(3) = learning rate
%       param(4) = preference for gems/earning        
%
%
% written:  ~#wem3#~ [20180423]
% modified: ~#wem3#~ [20191121]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % param(1) = inverse temperature
    param(1).name = 'inverse temperature'; 
    % parameters of the gamma prior
    param(1).hp = [1 5]; 
    % log density function for prior
    param(1).logpdf = @(x) sum(log(gampdf(x,param(1).hp(1),param(1).hp(2))));
    % log link function that transforms x from [0, +inf) to (-inf, +inf)
    param(1).link = @(x) log(x); 
    param(1).lb = 0;                % lower bound
    param(1).ub = 50;               % upper bound

    % param(2) = stickiness: perseveration parameter
    param(2).name = 'choice stickiness';
    % parameters of the normal prior
    param(2).hp = [0 10];
    % log density function for prior
    param(2).logpdf = @(x) sum(log(normpdf(x,param(2).hp(1),param(2).hp(2))));  
    % probit link function transforms x from [-5,5] to (-inf, +inf)
    param(2).link = @(x) 1./(1+exp(-x)); 
    param(2).lb = -5;               % lower bound
    param(2).ub =  5;               % upper bound

    % param(3) = learning rate
    param(3).name = 'learning rate'; % learning rate
    param(3).hp = [1.2 1.2]; % parameters of the beta prior
    param(3).logpdf = @(x) sum(log(betapdf(x,param(3).hp(1),param(3).hp(2))));
    % logit link function transforms x from [0,1] to (-inf, +inf)
    param(3).link = @(x) log(x) - log(1-x);
    param(3).lb = 0;               % lower bound
    param(3).ub = 1;               % upper bound

    % param(4) = preference for gems/earning
    param(4) = param(3);
    param(4).name = 'preference weight';

    return
